import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomeCenterConfigurationComponent } from './components/home-center-configuration/home-center-configuration.component';
import { HomeCenterSectionsComponent } from './components/home-center-sections/home-center-sections.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeCenterDevicesComponent } from './components/home-center-devices/home-center-devices.component';
import { FormsModule } from '@angular/forms';
import {MatButtonModule, MatCheckboxModule, MatSelectModule} from '@angular/material';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatInputModule} from '@angular/material/input';
import {MatSliderModule} from '@angular/material/slider';

@NgModule({
  declarations: [
    AppComponent,
    HomeCenterConfigurationComponent,
    HomeCenterSectionsComponent,
    HomeCenterDevicesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSelectModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatCardModule,
    MatToolbarModule,
    MatInputModule,
    MatSliderModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
