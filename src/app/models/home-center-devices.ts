export interface HomeCenterDevices {
    id: number;
    name: string;
    roomID: number;
    type: string;
    properties: any[];
    actions: any[];
    sortOrder: number;
}
