export interface HomeCenterRooms {

    id: number;
    name: string;
    sectionID: number;
    sortOrder: number;
}
