export interface HomeCenterSections {
    id: number;
    name: string;
    sortOrder: number;
}
