export interface HomeCenterConf {
    serialNumber: string;
    hcName: string;
    mac: string;
    softVersion: number;
    beta: boolean;
    zwaveVersion: number;
    timeFormat: number;
    zwaveRegion: string;
    serverStatus: number;
    defaultLanguage: string;
    sunsetHour: string;
    sunriseHour: string;
    hotelMode: boolean;
    updateStableAvailable: boolean;
    temperatureUnit: string;
    newestStableVersion: number;
    updateBetaAvailable: boolean;
    batteryLowNotification: boolean;
    smsManagement: boolean;
    date: string;
    timestamp: number;
    online: boolean;
    recoveryCondition: string;
}
