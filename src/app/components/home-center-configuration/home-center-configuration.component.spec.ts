import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCenterConfigurationComponent } from './home-center-configuration.component';

describe('HomeCenterConfigurationComponent', () => {
  let component: HomeCenterConfigurationComponent;
  let fixture: ComponentFixture<HomeCenterConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCenterConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCenterConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
