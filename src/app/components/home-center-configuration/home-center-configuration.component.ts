import { Component, OnInit } from '@angular/core';
import { HomeCenterConf } from 'src/app/models/home-center-conf';
import { HomeCenterService } from 'src/app/services/home-center.service';

@Component({
  selector: 'app-home-center-configuration',
  templateUrl: './home-center-configuration.component.html',
  styleUrls: ['./home-center-configuration.component.css']
})
export class HomeCenterConfigurationComponent implements OnInit {

  HMConf: any[];
  HMConfKeys: any;
  showConfig = false;
  buttonText = 'show Configuration';

  constructor(private homeCenterService: HomeCenterService) { }

  ngOnInit() {
    this.fetchHomeCenterConf();
  }


  fetchHomeCenterConf() {
    this.homeCenterService
      .fetchConf()
      .subscribe(
        result => {
          this.HMConf = Object.entries(result);
        });
  }

  toggleConfigSection() {
    if (this.showConfig) {
      this.buttonText = 'show Configuration';
      this.showConfig = false;
    } else {
      this.showConfig = true;
      this.buttonText = 'hide configuration';
    }
  }

  getOnlyImportantConf(item) {
    if (item[0] === 'serialNumber' || item[0] === 'mac' || item[0] === 'softVersion') {
      return true;
    } else {
      return false;
    }

  }

}
