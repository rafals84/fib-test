import { Component, OnInit } from '@angular/core';
import { HomeCenterService } from 'src/app/services/home-center.service';
import { HomeCenterSections } from 'src/app/models/home-center-sections';
import { HomeCenterRooms } from 'src/app/models/home-center-rooms';

@Component({
  selector: 'app-home-center-sections',
  templateUrl: './home-center-sections.component.html',
  styleUrls: ['./home-center-sections.component.css']
})
export class HomeCenterSectionsComponent implements OnInit {

  HMSections: HomeCenterSections;
  HMRooms: HomeCenterRooms;

  constructor(private homeCenterService: HomeCenterService, ) { }

  ngOnInit() {

  }

  fetchHomeCenterSections() {
    this.homeCenterService
      .fetchSections()
      .subscribe(
        result => {
          this.HMSections = result;
        });
  }

  fetchHomeCenterRooms() {
    this.homeCenterService
      .fetchRooms()
      .subscribe(
        result => {
          this.HMRooms = result;
        });
  }

  getSectionAndRooms() {
    this.fetchHomeCenterSections();
    this.fetchHomeCenterRooms();
  }


}
