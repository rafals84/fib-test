import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCenterSectionsComponent } from './home-center-sections.component';

describe('HomeCenterSectionsComponent', () => {
  let component: HomeCenterSectionsComponent;
  let fixture: ComponentFixture<HomeCenterSectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCenterSectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCenterSectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
