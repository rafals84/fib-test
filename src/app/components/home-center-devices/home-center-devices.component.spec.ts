import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCenterDevicesComponent } from './home-center-devices.component';

describe('HomeCenterDevicesComponent', () => {
  let component: HomeCenterDevicesComponent;
  let fixture: ComponentFixture<HomeCenterDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCenterDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCenterDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
