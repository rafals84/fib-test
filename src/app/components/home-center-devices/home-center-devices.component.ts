import { HomeCenterService } from 'src/app/services/home-center.service';
import { Component, OnInit, Input } from '@angular/core';
import { removeDebugNodeFromIndex } from '@angular/core/src/debug/debug_node';
import { HomeCenterDevices } from 'src/app/models/home-center-devices';


@Component({
  selector: 'app-home-center-devices',
  templateUrl: './home-center-devices.component.html',
  styleUrls: ['./home-center-devices.component.css']
})
export class HomeCenterDevicesComponent implements OnInit {


  @Input() roomId;
  HMDevices: HomeCenterDevices;
  deviceId: string;
  HMDevicesArr: any;
  suitableDevice = true;
  deviceValue = 0;


  constructor(private homeCenterService: HomeCenterService) { }

  ngOnInit() {
    this.fetchHomeCenterDevices();
  }

  fetchHomeCenterDevices() {
    this.homeCenterService
      .fetchDevices()
      .subscribe(
        result => {
          this.HMDevices = result;
        });
  }

  sendHomeCenterDevices(device, toggleTurn) {
    this.homeCenterService
      .callAtions(device, toggleTurn)
      .subscribe(
        result => {
        });
  }

  ifProperDeviceType(device): boolean {
    // return true;
    return (device.type === 'binary_light' || device.type === 'dimmable_light');
  }

  getDeviceProperies() {
    this.HMDevicesArr = Object.values(this.HMDevices);
    const deviceProps = this.HMDevicesArr.find(e => e.id === this.deviceId);
    return deviceProps;
  }

  turnToggle(turnOffOn) {
    const deviceProp = this.getDeviceProperies();

    this.homeCenterService
      .callAtions(deviceProp, turnOffOn)
      .subscribe(
        result => {
          // console.log(result);
        });
  }


  setDeviceValue() {
    const deviceProp = this.getDeviceProperies();
    this.homeCenterService
      .callAtions(deviceProp, 'turnOffOn', this.deviceValue.toString())
      .subscribe(
        result => {
          // console.log(result);
        });
  }

  setDevice() {

    if (this.findTypeById(this.deviceId) !== 'dimmable_light') {
      this.suitableDevice = true;
    } else {
      this.suitableDevice = false;
    }

  }

  findTypeById(deviceId) {
    this.HMDevicesArr = Object.values(this.HMDevices);
    const deviceProps = this.HMDevicesArr.find(e => e.id === deviceId);
    return deviceProps.type;
  }
}
