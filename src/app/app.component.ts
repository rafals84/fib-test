import { HomeCenterService } from './services/home-center.service';
import { Component, OnInit } from '@angular/core';
import { HomeCenterConf } from './models/home-center-conf';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';


  constructor(private homeCenterService: HomeCenterService) {

  }

  ngOnInit() {
  }




}
