import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { HomeCenterConf } from '../models/home-center-conf';
import { HomeCenterSections } from '../models/home-center-sections';
import { HomeCenterDevices } from '../models/home-center-devices';

@Injectable({
  providedIn: 'root'
})
export class HomeCenterService {

  homeCenterUrl = 'http://styx.fibaro.com:9999';
  sectionsUrl = 'assets/sections.json';
  roomsUrl = 'assets/rooms.json';
  devicesUrl = 'assets/devices.json';
  actionsUrl = '/api/callAction';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };
  constructor(private http: HttpClient) {

  }


  fetchConf(): Observable<HomeCenterConf> {
    return this.http.get<HomeCenterConf>(this.homeCenterUrl + '/api/settings/info');
  }

  fetchSections(): Observable<HomeCenterSections> {
    return this.http.get<HomeCenterSections>(this.sectionsUrl);
  }

  fetchRooms(): Observable<any> {
    return this.http.get<any>(this.roomsUrl);
  }

  fetchDevices(): Observable<HomeCenterDevices> {
    return this.http.get<HomeCenterDevices>(this.devicesUrl);
  }

  sendChangeDealAmount(): Observable<string> {
    const options = {
      params: new HttpParams().set('dealId', 'dealAmount.dealId.toString()')
        .set('amount', 'dealAmount.amount.toString()')
    };
    return this.http.post<string>(this.devicesUrl, null, options);

  }

  callAtions(deviceTurn: any, toggleTurn: string, setDimmValue?: string): Observable<string> {
    let options = {};
    if (setDimmValue) {
      options = { params: new HttpParams().set('deviceID', deviceTurn.id).set('name', 'setValue').set('arg1', setDimmValue) };
    } else {
      options = { params: new HttpParams().set('deviceID', deviceTurn.id).set('name', toggleTurn) };
    }

    return this.http.post<string>(this.actionsUrl, null, options);
  }

}
